$(function() {

    var App = (function(){

        return {
            init : function() {
                //DummyModule.init();
                
                FanatsApp.init();
            }
        }
    })()

    ,FanatsApp = (function(){
        return {
            init : function() {
                $("#fanats_start").on('click', function(){
                    $(".fanats__step").removeClass('active');
                    $(".fanats__step--step2").addClass('active');
                    $(".fanats__input").first().focus();
                    return false;
                });

                $("#phone").mask("8 (999) 999-99-99");
                $("#dob").mask("99/99/9999", {placeholder:"дд/мм/гггг"});

                $('.fanats__form').validate({
                    messages: {
                        firstname: "Пожалуйста, укажите Ваше имя",
                        surname: "Пожалуйста, укажите Вашу фамилию",
                        phone: "Пожалуйста, укажите номер телефона",
                        dob: "Пожалуйста, укажите Вашу дату рождения",
                        agree: "Пожалуйста, согласитесь с правилами конкурса",
                        email: {
                            required : "Пожалуйста, укажите Ваш электронный адрес",
                            email : "Пожалуйста, укажите корректный электронный адрес"
                        }
                    },
                    submitHandler: function(form) {
                        // сюда процесс сабмита вставляем
                        // $(form).ajaxSubmit();
                        
                        $(".fanats__step").removeClass('active');
                        $(".fanats__step--step3").addClass('active');
                        return false;
                    }
                });
            }
        }
    })()

    /**
     * Dummy Module Example
     */
    ,DummyModule = (function(){
        return {
            init : function() {
                // do something
            }
        }
    })()

    ;App.init();

});


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}